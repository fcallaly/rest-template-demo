package com.training.resttemplate.service;

import com.training.resttemplate.entity.PriceDataWrapper;
import com.training.resttemplate.repository.PriceDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PriceDataService {

    private static final Logger LOG = LoggerFactory.getLogger(PriceDataService.class);

    // This should be set through a controller endpoint, or from application.properties
    private String tickerToWatch = "AMZN";

    private double lastWatchedPrice;

    @Autowired
    private PriceDataRepository priceDataRepository;

    public PriceDataWrapper getForTicker(String ticker) {
        return priceDataRepository.getForTicker(ticker);
    }

    // This function demonstrates reading the price at regular intervals
    // Note: we MUST include @EnableSheduling on the main class (RestTemplateApplication.java)
    // Run this function every 5mins , the interval variable could be set in application.properties
    @Scheduled(fixedRateString="300000")
    public void checkPriceChange() {
        PriceDataWrapper priceDataWrapper = priceDataRepository.getForTicker(tickerToWatch);

        List<Double> closePrices = priceDataWrapper.getPriceData().getClose();
        double newLatestPrice = closePrices.get(closePrices.size() - 1);

        // print a simple message about the change in price
        // this is where more interesting things could happen
        logPriceChange(newLatestPrice);

        // update the saved price
        lastWatchedPrice = newLatestPrice;
    }

    private void logPriceChange(double newLatestPrice) {
        String message1 = "Price for " + tickerToWatch + " has ";
        String message2 = "from " + lastWatchedPrice + " to " + newLatestPrice;

        if(lastWatchedPrice == 0) {
            LOG.info("First time around => save the price and do nothing");
        }
        else if(newLatestPrice > lastWatchedPrice) {
            LOG.info(message1 + "increased " + message2);
        }
        else if (newLatestPrice < lastWatchedPrice) {
            LOG.info(message1 + "decreased " + message2);
        }
        else {
            LOG.info(message1 + "not changed");
        }
    }
}
