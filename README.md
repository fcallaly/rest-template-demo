## Demonstration of sending a HTTP request from Spring/Java

This project uses the Spring RestTemplate to send a HTTP GET request to one of the training price data services.

The response from the HTTP service will be in JSON. The RestTemplate can convert (marshall) that JSON into Java object(s).

The java objects that the JSON is converted to are in entities/

The HTTP GET Request is sent from respository/PriceDataService.java

The URL to send the GET request to is defined in application.properties

There is a small function added to the main class (RestTemplateApplication.java) to create the RestTemplate bean

More info about the RestTemplate can be found:

- https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/client/RestTemplate.html
- https://www.baeldung.com/rest-template
- https://www.baeldung.com/spring-resttemplate-post-json